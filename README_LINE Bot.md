# Name
Command from LINE

# About
LINEからLinux(今回はRaspberry Pi 4を使用)のコマンドを操作するベースプログラムです．
LINE Messaging APIとFirebaseを用いて，自作のLINE Botから特定の文字列を送信すると，対応したコマンドがLinux上で実行されます．
こちらは上記プログラムの設定～起動手順です．

# Objectives
・既定のコマンドを手軽に実行したかった．\
　→特定の文字列に任意のコマンドを対応させた．\
・ラズパイの画面出力をせずにコマンド操作したかった．\
　→ラズパイとネット環境さえあれば，LINE経由で既定の操作が可能になった．

# Installation
## 設定手順
### ソースコードを展開する

任意の場所にソースコード一式を展開する．

```
App/
├── backend // ラズパイ側のバックエンドプログラム
│ ├── index.js
│ └── package.json
├── database.rules.json // firebase database のルール定義
├── firebase.json // firebase の 設定
└── functions // firebase のコンテナ上で動作する BOT アプリ
├── index.js
└── package.json
```

### Firebase側の設定

1. プロジェクトの作成

    firebase コンソールからプロジェクトを作成する．
    [https://console.firebase.google.com/](https://console.firebase.google.com/)

    ```
    [設定内容]	
    プロジェクト名：sample	
    Google アナリティクス：有効	
    Google アナリティクスアカウント：新しいアカウントを作成	
    	アカウント名：firebase (任意)
    	アナリティクスの地域：日本
    	データ共有の設定と Google アナリティクスの利用規約	
    		□Googleアナリティクスデータの共有にデフォルト設定を使用します。※すべてのチェック外す
    		■Googleアナリティクス利用規約に同意します
    ```

2. プラン変更

    外部APIを利用するために，1 で作成したプロジェクトの料金プランを Blaze プランに変更する．

3. Webアプリの設定

    設定 > プロジェクトを設定 > 全般 タブ を開き，マイアプリ から ウェブアプリを追加する．

    ```
    [設定内容]	
    アプリのニックネーム：マイウェブアプリ (任意)	
    □このアプリの Firebase Hosting も設定します.　※チェック外す
    ```

    登録後，「コンソールへ進む」ボタンを押す．または，
    設定 > プロジェクトを設定 > 全般 タブ を開き，マイアプリ から ウェブアプリ の欄の 「マイウェブアプリ」を選択．

    Firebase SDK snippet の ◉構成 を選択して表示される  firebaseConfig を以下のソースコード内の firebaseConfig に設定する。

    ```
    ・functions/index.js	
    ・backend/index.js
    ```
### LINE Developers側の設定

1. プロバイダーの作成
Line Developer にログインする．
[https://developers.line.biz/ja/](https://developers.line.biz/ja/)

   プロバイダーを作成する．	

    ```
    [設定内容]	
    名前：sample (任意)
    ```

2. チャンネルの作成

    1.で作成した プロバイダーの「チャンネル設定」タブを開き，チャンネルを作成する．

    ```
    [設定内容]
    チャンネルの種類：Messaging API
    プロバイダー：sample (1 で作成したもの)
    チャンネル名：sampleChannel (任意)
    チャンネル説明：sample (任意)
    大業種：業種選択
    小業種：業種選択
    メールアドレス：あなたのメールアドレス
    プライバシーポリシーURL ：なし(任意)
    サービス利用規約URL：なし(任意)
    ```
    
    チャンネル基本設定 タブを開き，チャネルシークレット を確認する． \
    チャンネルシークレットを functions/index.js の lineConfig.channelSecret に設定する． 

    Messaging API設定 タブを開き、チャネルアクセストークン を発行する． \
    チャンネルアクセストークンを functions/index.js の lineConfig.channelAccessToken に設定する． 

    Messaging API設定 タブを開き，QRコードを確認する． \
    LINEアプリで，QRコードからこのチャンネルを友達追加する． 

    Messaging API設定 タブを開き，Webhook設定を行う．

    ```
    [設定内容]
    Webhook URL ：https://(firebase project id *1).web.app/webhook
    Webhookの利用：有効

    *1) firebase で作成したプロジェクトページの
    設定 > プロジェクトを設定 > 全般 タブ を開き，「プロジェクト ID」から確認できる．例：sample-d453e

    ※ Webhook URL 付近の「検証」ボタンを押さなければ値が反映されていなかった．(検証 の結果はエラーになるが無視)
    念のため，設定後トップページから再度この画面に遷移して，Webhook URL の値が更新されているか確認してください．
    ```

    Messaging API設定 タブを開き，応答メッセージを無効にする． \
    無効にしないと「メッセージありがとうございます〜」(デフォルト設定) のような応答が返ってくる．

## 起動手順

Raspberry Pi で以下の操作をし，本プログラムを実行する．

### Node.js 10 のインストール

```
$ curl -sL [https://deb.nodesource.com/setup_10.x](https://deb.nodesource.com/setup_10.x) | sudo -E bash -
$ sudo apt-get install -y nodejs
$ node -v
v10.22.0

参考：https://github.com/nodesource/distributions/blob/master/README.md
```

### Bot アプリのデプロイ

**Firebaseの初期設定**

```
# firebase の CLIツールをインストール
$ cd App/functions
$ sudo npm install -g firebase-tools

# ログイン
$ firebase login --no-localhost
i  Firebase optionally collects CLI usage and error reporting information to help improve our products. Data is collected in accordance with Google's privacy policy (https://policies.google.com/privacy) and is not used to identify you.

? Allow Firebase to collect CLI usage and error reporting information? (Y/n) Yを入力してEnter

Visit this URL on this device to log in:
https://accounts.google.com/o/oauth2/auth?client_id=...

? Paste authorization code here:
```

ブラウザを開き，上記で表示される URL にアクセスし，Googleアカウントにログインする． \
ログイン後に表示されるコードを ，「Paste authorization code here: 」に入力して Enter \
\
以下のコマンドを実行し，作成した project を使用する．

```
$ firebase use (project id)

# 例：firebase use sample-d453e
```

**デプロイ**

```
$ cd App/functions
$ npm install
$ cd ../
$ firebase deploy

=== Deploying to 'sample-d453e'...

i  deploying database, functions, hosting
i  database: checking rules syntax...
✔  database: rules syntax for database sample-d453e is valid
i  functions: ensuring required API cloudfunctions.googleapis.com is enabled...
i  functions: ensuring required API cloudbuild.googleapis.com is enabled...
✔  functions: required API cloudbuild.googleapis.com is enabled
✔  functions: required API cloudfunctions.googleapis.com is enabled
i  functions: preparing functions directory for uploading...
i  functions: packaged functions (29.59 KB) for uploading
✔  functions: functions folder uploaded successfully
i  hosting[sample-d453e]: beginning deploy...
i  hosting[sample-d453e]: found 3 files in functions
✔  hosting[sample-d4535e]: file upload complete
i  database: releasing rules...
✔  database: rules for database sample-d453e released successfully
i  functions: creating Node.js 10 function app(us-central1)...
✔  functions[app(us-central1)]: Successful create operation. 
Function URL (app): https://us-central1-sample-d453e.cloudfunctions.net/app
i  hosting[sample-d453e]: finalizing version...
✔  hosting[sample-d453e]: version finalized
i  hosting[sample-d453e]: releasing new version...
✔  hosting[sample-d453e]: release complete

✔  Deploy complete!

Project Console: https://console.firebase.google.com/project/sample-d453e/overview
Hosting URL: https://sample-d453e.web.app
```

**バックエンドプログラムの実行**

```
$ cd App/backend
$ npm install
$ npm start
```

最初は何もログが表示されません． \
もし終了する場合は Ctrl + C で強制終了してください．

## Usage
**動作確認**

LINEからsampleChannelに start と入力して送信する．

1. LINE の方に，「start 要求を発行しました」というデバッグメッセージが送信されることを確認する．
2. バックエンドプログラムで，start コマンドを判定して lsコマンドを実行し，実行結果のログが表示されることを確認する．

# Note
このままだと
```
$ npm start
```
を都度実行しなければなりません．
そのため，上記コマンドをsystemdサービスとして登録&起動時に自動実行しておくと便利です．

# References
[LINE と Firebase とラズパイを繋いでみたよ☆](https://qiita.com/kt-sz/items/c008084300a978516153) \
[LINEを「ラズパイのターミナル化＆スマートホームのフロントエンド化(ChatOps風)」してみたよ♪](https://qiita.com/kt-sz/items/9adcb02a3b3a6cf3dd7b)
# License
Command from LINE is under MIT license.

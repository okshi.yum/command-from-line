'use strict';

const functions = require('firebase-functions');
const express = require('express');
const line = require('@line/bot-sdk');
const firebase = require("firebase");

// LINE DevelopersよりMessaging APIの情報をペースト
const lineConfig = {
    channelSecret: '',
    channelAccessToken: ''
};
const client = new line.Client(lineConfig);

// set a Firebase SDK snippet
const firebaseConfig = {
  apiKey: "",
  authDomain: "",
  databaseURL: "",
  projectId: "",
  storageBucket: "",
  messagingSenderId: "",
  appId: "",
  measurementId: ""
};
firebase.initializeApp( firebaseConfig );

// DB初期化
var db = firebase.database();
var ref = db.ref( "/" );
ref.set({
  line: {
    message: {
      request : "request string",
      isRequest: false
    }
  }
});

const app = express();
app.post('/webhook', line.middleware(lineConfig), (req, res) => {
    console.log(req.body.events);
    Promise
      .all(req.body.events.map(handleEvent))
      .then((result) => res.json(result))
      .catch((result) => console.log('error!!!'));
});

async function handleEvent(event) {
  if (event.type !== 'message' || event.message.type !== 'text') {
    return Promise.resolve(null);
  }

  // request
  //   - update firebase database
  ref = db.ref( "/line/message" );
  ref.update({
    "request": event.message.text,
    "isRequest": true
  });

  // send debug log
  return client.replyMessage(event.replyToken, {
    type: 'text',
    text: event.message.text + ' リクエストを発行しました'
  });
}

exports.app = functions.https.onRequest(app);
